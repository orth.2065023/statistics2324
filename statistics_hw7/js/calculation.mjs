// Whole-script strict mode syntax
"use strict";

// Calculates a single trajectory based on the desired mode (score, cumulated, relative, normalized, random)
export function calculateTrajectory(N, mode, param) {
    let trajectory = [];
    let value = 0;
    let previousValue = param[0];
    let mu = param[1];
    let sigma = param[2];
    let theta = param[3];
    let kappa = param[4];
    let phi = param[5];
    let nu = param[6];
    let xi = param[7];
    let rho = param[8];
    let alpha = param[9];
    let beta = param[10];
    let zeta = param[11];
    let eta = param[12];
    let equation = param[13];
    let dt = 1/N;
    


    for (let i = 0; i < N; i++) {
        if (mode == 1) // mode = ArithmeticBrownianMotion
        {
            if (i > 0){
                previousValue = trajectory[i-1].Y;
            }

            value = simulateArithmeticBrownianMotion(previousValue, mu, sigma, dt);
            trajectory.push({ X: i + 1, Y: value });
        }
        else if (mode == 2) // mode = GeometricBrownianMotion
        {
            if (i > 0){
                previousValue = trajectory[i-1].Y;
            }
            
            value = simulateGeometricBrownianMotion(previousValue, mu, sigma, dt);
            trajectory.push({ X: i + 1, Y: value });
        }
        else if (mode == 3) // mode = Ornstein–Uhlenbeck (Mean-Reverting)
        {
            if (i > 0){
                previousValue = trajectory[i-1].Y;
            }
            
            value = simulateOrnsteinUhlenbeck(previousValue, theta, mu, sigma, dt);
            trajectory.push({ X: i + 1, Y: value });
        }
        else if (mode == 4) // mode = Vasicek
        {
            if (i > 0){
                previousValue = trajectory[i-1].Y;
            }
            
            value = simulateVasicek(previousValue, kappa, theta, sigma, dt);
            trajectory.push({ X: i + 1, Y: value });
        }
        else if (mode == 5) // mode = Hull-White 1F
        {
            if (i > 0){
                previousValue = trajectory[i-1].Y;
            }
            
            value = simulateHullWhiteOneFactor(previousValue, kappa, theta, sigma, dt);
            trajectory.push({ X: i + 1, Y: value });
        }
        else if (mode == 6) // mode = Cox–Ingersoll–Ross
        {
            if (i > 0){
                previousValue = trajectory[i-1].Y;
            }
            
            value = simulateCIR(previousValue, kappa, theta, sigma, dt);
            trajectory.push({ X: i + 1, Y: value });
        }
        else if (mode == 7) // mode = Black-Krasinski
        {
            if (i > 0){
                previousValue = trajectory[i-1].Y;
            }
            
            value = simulateBK(previousValue, phi, theta, sigma, dt);
            trajectory.push({ X: i + 1, Y: value });
        }
        else if (mode == 8) // mode = Heston
        {
            if (i > 0){
                previousValue = trajectory[i-1].Y;
            }
            let values = simulateHeston(previousValue, nu, xi, rho, kappa, theta, mu, dt);
            value = values[0];
            nu = values[1];
            trajectory.push({ X: i + 1, Y: value });
        }
        else if (mode == 9) // mode = Chen
        {
            if (i > 0){
                previousValue = trajectory[i-1].Y;
            }
            let values = simulateChen(previousValue, nu, alpha, beta, eta, sigma, zeta, kappa, theta, mu, dt);
            value = values[0];
            theta = values[1];
            sigma = values[2];
            trajectory.push({ X: i + 1, Y: value });
        }
        else if (mode == 13) // mode = Costum
        {
            if (i > 0){
                previousValue = trajectory[i-1].Y;
            }
            value = simulateSDE(previousValue, equation, dt);
            trajectory.push({ X: i + 1, Y: value });
        }
    }
    return trajectory;
}

// Frequency array to be used by BarChart
export function countFrequency(trajectoryValues, intervals, minValue, maxValue) {
    
    let frequency = new Array(intervals).fill(0);

    let intervalSize = (maxValue - minValue) / intervals;

    for (let i = 0; i < trajectoryValues.length; i++) {
        let interval = Math.floor((trajectoryValues[i] - minValue) / intervalSize);
        let index = 0;

        if (interval == frequency.length)
        {
            interval--;
        }
        
        index = interval;
        
        
        frequency[index]++;
    }
    return frequency;
}

// Generate M number of trajectories
export function generateNumberOfTrajectories(M, N, mode, param){
    let trajectories_score = [];
    let trajectories = [];

    // For every M calculate trajectory
    for (let i = 0; i < M; i++) {
        trajectories_score[i] = calculateTrajectory(N, mode, param);
    }
    trajectories[0] = trajectories_score;
    return trajectories;
}

// Function to generate random numbers from a Gaussian distribution
function gaussianRandom() {
    let u1, u2;
    let z0, z1;

    do {
        u1 = Math.random();
        u2 = Math.random();
    } while (u1 <= 1e-6); // Ensure u1 is not too close to 0

    z0 = Math.sqrt(-2.0 * Math.log(u1)) * Math.cos(2 * Math.PI * u2);
    // z1 = Math.sqrt(-2.0 * Math.log(u1)) * Math.sin(2 * Math.PI * u2);

    return z0;
}

function simulateArithmeticBrownianMotion(X, mu, sigma, deltaT) {
    const epsilon = gaussianRandom(); // Function to generate random number from standard normal distribution
    return X + mu * deltaT + sigma * epsilon * Math.sqrt(deltaT);
}
  
function simulateGeometricBrownianMotion(X, mu, sigma, deltaT) {
    const epsilon = gaussianRandom();
    return X + (mu * X * deltaT + sigma * X * epsilon * Math.sqrt(deltaT));
}

function simulateOrnsteinUhlenbeck(X, theta, mu, sigma, deltaT) {
    const epsilon = gaussianRandom();
    return X + theta * (mu - X) * deltaT + sigma * epsilon * Math.sqrt(deltaT);
}

function simulateVasicek(X, kappa, theta, sigma, deltaT) {
    const epsilon = gaussianRandom();
    return X + kappa * (theta - X) * deltaT + sigma * Math.sqrt(deltaT) * epsilon;
}

function simulateHullWhiteOneFactor(X, kappa, theta, sigma, deltaT) {
    const epsilon = gaussianRandom();
    return X + (theta - kappa*X) * deltaT + sigma * Math.sqrt(deltaT) * epsilon;
    
}
 
function simulateCIR(X, kappa, theta, sigma, deltaT) {
    const epsilon = gaussianRandom();
    return X + kappa * (theta - X) * deltaT + sigma * Math.sqrt(X * deltaT) * epsilon;
}

function simulateBK(X, phi, theta, sigma, deltaT) {
    const epsilon = gaussianRandom();
    return X + (theta - phi * Math.log(X)) * deltaT + sigma * Math.sqrt(X * deltaT) * epsilon;
}

function simulateHeston(X, nu, xi, rho, kappa, theta, mu, dt) {

    const dW1 = Math.sqrt(dt) * gaussianRandom();
    const dW2 = rho * dW1 + Math.sqrt(1 - rho * rho) * Math.sqrt(dt) * gaussianRandom();

    const X1 = X + mu * X * dt + Math.sqrt(nu) * X * dW1;
    const V1 = nu + kappa * (theta - nu) * dt + xi * Math.sqrt(nu) * dW2;
    const values = [X1, V1];
    return values;
}

function simulateChen(X, nu, alpha, beta, eta, sigma, zeta, kappa, theta, mu, dt) {
    const dW1 = Math.sqrt(dt) * gaussianRandom();
    const dW2 = Math.sqrt(dt) * gaussianRandom();
    const dW3 = Math.sqrt(dt) * gaussianRandom();

    const X1 = X + kappa * (theta - X) * dt + Math.sqrt(X*sigma) * dW1;
    const theta1 = theta + nu * (zeta - theta) * dt + alpha * Math.sqrt(theta) * dW2;
    const sigma1 = sigma + mu * (beta - sigma) * dt + eta * Math.sqrt(sigma) * dW3;

  
    const values = [X1, theta1, sigma1];
    return values;
}

function simulateSDE(S, equation, dt) {
    const dW = Math.sqrt(dt) * gaussianRandom();
    
    let parts = equation.split('=');
    if (parts.length !== 2) {
        throw new Error('Invalid equation format');
    }
    // Example parts[1] -> (1.5 * S) * dt + (2 * S) * dW
    let variables = {S: S, dt: dt, dW: dW};
    let calculation = evaluateSDE(parts[1], variables);
    return S + calculation;
}

function evaluateSDE(expression, variables) {
    // Create a scope with variable values
    const scope = Object.assign({}, variables);

    // Evaluate the expression
    const result = math.evaluate(expression, scope);
    //const result = math.evaluate("2 + 3 * 4");

    return result;
}

// Test function to observe the output
function calculation() {
    
}

calculation();