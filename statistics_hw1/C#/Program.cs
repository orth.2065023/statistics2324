﻿using System;
using SkiaSharp;

namespace ShapesDrawingConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            // Set the size of the canvas
            int width = 720;
            int height = 1280;

            // Create a bitmap as the drawing canvas with white background
            using (var surface = SKSurface.Create(width, height, SKImageInfo.PlatformColorType, SKAlphaType.Premul))
            {
                // Get the canvas from the surface
                var canvas = surface.Canvas;

                // Set background color to white
                canvas.Clear(SKColors.White);

                // Draw a line
                using (var paint = new SKPaint { Color = SKColors.Blue, StrokeWidth = 5 })
                {
                    canvas.DrawLine(100, 160, 620, 160, paint);
                }

                // Draw a point
                using (var paint = new SKPaint { Color = SKColors.Red, StrokeWidth = 10 })
                {
                    canvas.DrawPoint(360, 480, paint);
                }

                // Draw a circle
                using (var paint = new SKPaint { Color = SKColors.Green, StrokeWidth = 5, IsStroke = true })
                {
                    canvas.DrawCircle(360, 800, 100, paint);
                }

                // Draw a rectangle
                using (var paint = new SKPaint { Color = SKColors.Orange, StrokeWidth = 5, IsStroke = true })
                {
                    var rect = new SKRect(100, 980, 620, 1260);
                    canvas.DrawRect(rect, paint);
                }

                // Save the canvas as an image file (optional)
                using (var image = surface.Snapshot())
                using (var data = image.Encode(SKEncodedImageFormat.Png, 100))
                using (var stream = System.IO.File.OpenWrite("shapes.png"))
                {
                    data.SaveTo(stream);
                }

                Console.WriteLine("Shapes drawn and saved as shapes.png.");
            }
        }
    }
}
