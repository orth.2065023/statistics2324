const { createCanvas } = require('canvas');
const fs = require('fs');

// Set canvas dimensions
const canvasWidth = 720;
const canvasHeight = 1280;

// Create a canvas
const canvas = createCanvas(canvasWidth, canvasHeight);
const ctx = canvas.getContext('2d');

// Set the background color to white
ctx.fillStyle = 'white';
ctx.fillRect(0, 0, canvasWidth, canvasHeight);

// Set the vertical spacing between shapes
const spacing = 20;

// Calculate the height for each shape to fit them vertically
const totalHeight = canvasHeight - 5 * spacing;
const shapeHeight = totalHeight / 4;

// Calculate the center of the canvas
const centerX = canvasWidth / 2;

// Draw a line
ctx.beginPath();
ctx.moveTo(centerX, spacing);
ctx.lineTo(centerX, spacing + shapeHeight);
ctx.strokeStyle = 'blue';
ctx.lineWidth = 2;
ctx.stroke();

// Draw a point
ctx.beginPath();
ctx.arc(centerX, 160 + spacing * 2 + shapeHeight, 2, 0, 2 * Math.PI);
ctx.fillStyle = 'red';
ctx.fill();

// Draw a circle
ctx.beginPath();
ctx.arc(centerX, spacing * 3 + 2 * shapeHeight + shapeHeight / 2, shapeHeight / 2, 0, 2 * Math.PI);
ctx.strokeStyle = 'green';
ctx.stroke();

// Draw a rectangle
ctx.beginPath();
ctx.rect(centerX - shapeHeight / 2, spacing * 4 + 3 * shapeHeight, shapeHeight, shapeHeight / 2);
ctx.strokeStyle = 'orange';
ctx.stroke();

// Save the canvas as an image file
const buffer = canvas.toBuffer('image/png');
fs.writeFileSync('shapes.png', buffer);

console.log('Shapes drawn on a white background and saved as shapes.png.');
