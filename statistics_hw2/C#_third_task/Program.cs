﻿using System;

class Program
{
    static void Main()
    {
        uniform_random_intervals(1000, 10);
        uniform_random_intervals(100000, 10);
        uniform_random_intervals(10000000, 10);
    }

    static void uniform_random_intervals (int N, int k)
    {
        // N is number of random variates to generate

        // k is number of class intervals

        // Seed for random number generation
        Random rand = new Random();

        // Array to store counts for each interval
        int[] intervalCounts = new int[k];

        // Generate N uniform random variates in [0, 1) and determine class intervals
        for (int i = 0; i < N; i++)
        {
            double randomValue = rand.NextDouble(); // Generate a random value between 0 and 1
            int intervalIndex = (int)(randomValue * k); // Determine the interval index

            // Increment the count for the corresponding interval
            intervalCounts[intervalIndex]++;
        }

        // Display the distribution
        Console.WriteLine($"--- Interval Counts  (N = {N}, k = {k}) ---");
        for (int i = 0; i < k; i++)
        {
            double lowerBound = i * 1.0 / k;
            double upperBound = (i + 1) * 1.0 / k;
            Console.WriteLine($"Interval [{lowerBound:F2}, {upperBound:F2}): {intervalCounts[i]} -> {((double)intervalCounts[i]/N)*100:F3} %");
        }
        Console.WriteLine("-----------------------------------------------");
        Console.WriteLine();
        Console.WriteLine();
    }
}
