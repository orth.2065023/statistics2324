﻿using System;
using System.Collections;
using System.Collections.Generic;

class Program
{
    static void Main(string[] args)
    {
        //func_array();
        //func_list();
        //func_dictionary();
        //func_sortedlist();
        //func_hashset();
        //func_sortedset();
        //func_queue();
        //func_stack();
        func_linkedlist();
    }
    static void func_array()
    {   
        int[] array = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        // Loop and print each element
        foreach (var item in array)
        {
            // ...
        }

        Console.WriteLine($"Array elements: \n[{string.Join(", ", array)}]\n");

        // Add element to array
        array = array.Append(11).ToArray(); 

        // Remove element from array
        array = array.Where(val => val != 8).ToArray(); 
        
        // Get element by index from array
        int element = array[2]; 
        Console.WriteLine("Array element with index 2 is: " + element);

        // Check existence in array by value
        bool exists = array.Contains(4); 
        Console.WriteLine("Element with value 4 is existent in array: " + exists);
        
        // Print each element after modifications
        Console.WriteLine($"\nArray elements after modifications: \n[{string.Join(", ", array)}]\n");
    }
    static void func_list()
    {
        List<int> list = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        
        // Loop and print each element
        foreach (var item in list)
        {
            // ... 
        }

        Console.WriteLine($"List elements: \n[{string.Join(", ", list)}]\n");

        // Add element to list
        list.Add(11); 

        // Remove element from list
        list.Remove(8); 

        // Get list element by index
        int element = list[2]; 
        Console.WriteLine("List element with index 2 is: " + element);
        
        // Check list elements existence by value
        bool exists = list.Contains(4); 
        Console.WriteLine("Element with value 4 is existent in list: " + exists);
        
        // Print each element after modifications
        Console.WriteLine($"\nList elements after modifications: \n[{string.Join(", ", list)}]\n");
    }
    static void func_dictionary()
    {
        Dictionary<string, int> dictionary = new Dictionary<string, int>
        {
            { "One", 1 },
            { "Two", 2 },
            { "Three", 3 },
            { "Four", 4 },
            { "Five", 5 },
            { "Six", 6 },
            { "Seven", 7 },
            { "Eight", 8 },
            { "Nine", 9 },
            { "Ten", 10 }
        };

        // Loop and print each element
        foreach (var item in dictionary) 
        {
            // ...
        }

        Console.WriteLine($"Dictionary elements: \n[{string.Join(", ", dictionary)}]\n");

        // Add element to dictionary
        dictionary.Add("Eleven", 11); 
        
        // Remove element from dictionary
        dictionary.Remove("Eight"); 

        // Get dictionary element value by key
        int element = dictionary["Three"]; 
        Console.WriteLine("Dictionary element value with key Three is: " + element);

        // Check existence of element in dictionary by key
        bool exists = dictionary.ContainsKey("Four");
        Console.WriteLine("Element with key Four is existent in dictionary: " + exists);
        
        // Print each element after modifications
        Console.WriteLine($"\nDictionary elements after modifications: \n[{string.Join(", ", dictionary)}]\n");
    }
    static void func_sortedlist()
    {
        SortedList<string, int> sortedList = new SortedList<string, int>
        {
            { "One", 1 },
            { "Two", 2 },
            { "Three", 3 },
            { "Four", 4 },
            { "Five", 5 },
            { "Six", 6 },
            { "Seven", 7 },
            { "Eight", 8 },
            { "Nine", 9 },
            { "Ten", 10 }
        };

        // Loop and print each element
        foreach (var item in sortedList)
        {
            // ...
        }

        Console.WriteLine($"SortedList elements: \n[{string.Join(", ", sortedList)}]\n");

        // Add element to sortedList
        sortedList.Add("Eleven", 11); 

        // Remove element from sortedList
        sortedList.Remove("Eight");

        // Get element value by key
        int element = sortedList["Three"]; 
        Console.WriteLine("SortedList element value with key Three is: " + element);

        // Check existence of element key in sortedList
        bool exists = sortedList.ContainsKey("Four");
        Console.WriteLine("Element with key Four is existent in sortedList: " + exists);

        // Print each element after modifications
        Console.WriteLine($"\nSortedList elements after modifications: \n[{string.Join(", ", sortedList)}]\n");
    }
    static void func_hashset()
    {
        HashSet<int> hashSet = new HashSet<int>
        {
            1, 2, 3, 4, 5, 6, 7, 8, 9, 10
        };

        // Loop and print each element
        foreach (var item in hashSet)
        {
            // ...
        }

        Console.WriteLine($"HashSet elements: \n[{string.Join(", ", hashSet)}]\n");

        // Adding new elements (one duplicate and one new) to the hashSet
        hashSet.Add(5);
        hashSet.Add(11);

        // Removing an element from the hashSet
        hashSet.Remove(8);

        // Get the value by index is not applicable in hashSets since the elements have no order

        // Check if a value exists in the hashSet
        bool exists = hashSet.Contains(4);
        Console.WriteLine("Element with value 4 is existent in hashSet: " + exists);

        // Print each element after modifications
        Console.WriteLine($"\nHashSet elements after modifications: \n[{string.Join(", ", hashSet)}]\n");
    }
    static void func_sortedset()
    {
        SortedSet<int> sortedSet = new SortedSet<int>
        {
            1, 2, 3, 4, 5, 6, 7, 8, 9, 10
        };

        // Loop and print each element
        foreach (var item in sortedSet)
        {
            // ...
        }

        Console.WriteLine($"SortedSet elements: \n[{string.Join(", ", sortedSet)}]\n");

        // Adding new elements (one duplicate and two new unordered variables) to the sortedSet
        sortedSet.Add(5);
        sortedSet.Add(12);
        sortedSet.Add(11);

        // Removing an element from the sortedSet
        sortedSet.Remove(8);

        // Get the value by index is only possible by workaround since the set is build like a tree but on large sets this is very slow
        int element = 0;
        var enumerator = sortedSet.GetEnumerator();
        for (int i = 0; i < 2 && enumerator.MoveNext(); i++)
        {
            element = enumerator.Current;
        }
        Console.WriteLine("Element value on second position is: " + element);

        // Check if a value exists in the sortedSet
        bool exists = sortedSet.Contains(4);
        Console.WriteLine("Element with value 4 is existent in sortedSet: " + exists);

        // Print each element after modifications
        Console.WriteLine($"\nSortedSet elements after modifications: \n[{string.Join(", ", sortedSet)}]\n");
    }
    static void func_queue()
    {
        Queue<int> queue = new Queue<int>(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });

        //Loop and print each element
        foreach (var item in queue)
        {
            // ...
        }

        Console.WriteLine($"Queue elements: \n[{string.Join(", ", queue)}]\n");

        // Add element to the queue
        queue.Enqueue(11); 

        // Remove element from the queue (FIFO)
        queue.Dequeue(); 

        // Access elements by index in a queue is not applicable. 
        // To access a specific element, dequeue elements until desired element.

        // Check if element exists in the queue
        bool exists = queue.Contains(4); 
        Console.WriteLine("Element with value 4 is existent in Queue: " + exists);
       
        // Print each element after modifications
        Console.WriteLine($"\nQueue elements after modifications: \n[{string.Join(", ", queue)}]\n");
    }
    static void func_stack()
    {
        Stack<int> stack = new Stack<int>(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });

        // Loop and print each element
        foreach (var item in stack) 
        {
            // ...
        }

        Console.WriteLine($"Stack elements: \n[{string.Join(", ", stack)}]\n");

        // Add element to the stack
        stack.Push(11); 

        // Remove element from the stack (LIFO)
        stack.Pop(); 

        // Direct access elements by index in stack not applicable. 
        // To access a specific element, pop elements until the desired element.

        // Check if element exists in the stack
        bool exists = stack.Contains(4); 
        Console.WriteLine("Element with value 4 is existent in Stack: " + exists);

        // Print each element after modifications
        Console.WriteLine($"\nStack elements after modifications: \n[{string.Join(", ", stack)}]\n");
    }
    static void func_linkedlist()
    {
        LinkedList<int> linkedList = new LinkedList<int>(new[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });

        // Loop and print each element
        foreach (var item in linkedList)
        {
            // ...
        }

        Console.WriteLine($"LinkedList elements: \n[{string.Join(", ", linkedList)}]\n");

        // Add new element to linkedList
        linkedList.AddLast(11);

        // Remove element from linkedList
        var nodeToRemove = linkedList.Find(8);
        if (nodeToRemove != null)
        {
            linkedList.Remove(nodeToRemove);
        }

        // Access element by index is not applicable since 
        // linkedlist is not stored in contiguous memory locations

        // Check existence of element in linkedList
        bool exists = linkedList.Contains(4);
        Console.WriteLine("Element with key Four is existent in LinkedList: " + exists);

        // Print each element after modifications
        Console.WriteLine($"\nLinkedList elements after modifications: \n[{string.Join(", ", linkedList)}]\n");
    }
}
