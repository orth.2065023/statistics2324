// Whole-script strict mode syntax
"use strict";

function func_array() {
    
    // Define and init array
    const array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

    // Loop through each element
    array.forEach(function(item) {
       // ....
    });

    // Print each elemen
    console.log("Array elements:\n" + "[" + array.toString() + "]");
    console.log("");

    // Add element to array
    array.push(11);

    // Remove element 8 from array
    let index = array.indexOf(8);
    if (index !== -1) {
      array.splice(index, 1);
    }

    // Get element by index from array
    let element = array[2];
    console.log("Array element with index 2 is: " + element);

    // Check existence in array by value
    let exists = array.includes(4);
    console.log("Array element with value 4 is existent: " + exists);

    // Print each element after modifications
    console.log("");
    console.log("Array elements after modification:\n" + "[" + array.toString() + "]");
    
}

//func_list() not applicable in JavaScript since we can dynamically expand arrays 

function func_dictionary() {
    let dictionary = {
        "One": 1,
        "Two": 2,
        "Three": 3,
        "Four": 4,
        "Five": 5,
        "Six": 6,
        "Seven": 7,
        "Eight": 8,
        "Nine": 9,
        "Ten": 10
    };

    // Loop and print each element
    let keyValuePairs = [];

    for (let i = 0; i < Object.keys(dictionary).length; i++) {
        keyValuePairs.push("[" + Object.keys(dictionary)[i] + ", " + Object.values(dictionary)[i]+ "]");
    }
    console.log("Dictionary elements:\n" + "[" + keyValuePairs.join(", ") + "]\n");
    // Add element to dictionary
    dictionary["Eleven"] = 11;

    // Remove element from dictionary
    delete dictionary["Eight"];

    // Get dictionary element value by key
    let element = dictionary["Three"];
    console.log("Dictionary element value with key 'Three' is: " + element);

    // Check existence of element in dictionary by key
    let exists = "Four" in dictionary;
    console.log("Dictionary element with key 'Four' is existent: " + exists);
    
    // Print each element after modifications
    keyValuePairs = [];

    for (let i = 0; i < Object.keys(dictionary).length; i++) {
        keyValuePairs.push("[" + Object.keys(dictionary)[i] + ", " + Object.values(dictionary)[i]+ "]");
    }
    console.log("\nDictionary elements after modifications:\n" + "[" + keyValuePairs.join(", ") + "]\n");
}

class SortedList {
    constructor(initialItems = []) {
        this.items = [...initialItems];
        this.sortItems();
    }

    sortItems() {
        this.items.sort((a, b) => {
            if (a.key < b.key) return -1;
            if (a.key > b.key) return 1;
            return 0;
        });
    }

    add(key, value) {
        this.items.push({ key, value });
        this.sortItems();
    }

    remove(key) {
        const index = this.items.findIndex(item => item.key === key);
        if (index !== -1) {
            this.items.splice(index, 1);
        }
    }

    get(key) {
        const item = this.items.find(item => item.key === key);
        return item ? item.value : undefined;
    }

    containsKey(key) {
        return this.items.some(item => item.key === key);
    }

    print() {
        let result = this.items.map(item => `[${item.key}, ${item.value}]`).join(', ');
        console.log("[" + result + "]\n");
    }    
}

function func_sortedlist() {
    // Initialize SortedList with initial values
    const initialItems = [
        { key: "One", value: 1 },
        { key: "Two", value: 2 },
        { key: "Three", value: 3 },
        { key: "Four", value: 4 },
        { key: "Five", value: 5 },
        { key: "Six", value: 6 },
        { key: "Seven", value: 7 },
        { key: "Eight", value: 8 },
        { key: "Nine", value: 9 },
        { key: "Ten", value: 10 }
    ];

    const sortedList = new SortedList(initialItems);
    
    // Loop and print each element
    console.log("SortedList elements: ")
    sortedList.print();

    // Add element to sortedList
    sortedList.add("Eleven", 11);

    // Remove element from sortedList
    sortedList.remove("Eight");

    // Get element value by key in sortedList
    console.log("Value for key Three:", sortedList.get("Three"));

    // Check existence of element key in sortedList
    console.log("Key Four exists in sortedList:", sortedList.containsKey("Four"));
    console.log("");

    // Print each element after modification
    console.log("SortedList elements after modifications: ")
    sortedList.print();
}

function func_hashset() {
    // Create a Set with initial elements
    const hashSet = new Set([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);

    // Loop and print each element
    hashSet.forEach(item => {
        // ...
    });

    console.log("HashSet elements: [" + Array.from(hashSet).join(', ') + "]\n");

    // Adding new elements (one duplicate and one new) to the Set
    hashSet.add(5);
    hashSet.add(11);

    // Removing an element from the Set
    hashSet.delete(8);

    // Get the value by index is not applicable in hashSets since the elements have no order

    // Check if a value exists in the Set
    const exists = hashSet.has(4);
    console.log("Element with value 4 is existent in Set: " + exists);

    // Print each element after modifications
    console.log("\nHashSet elements after modification: [" + Array.from(hashSet).join(', ') + "]\n");
}

class SortedSet {
    constructor(initialValues = []) {
        this.set = new Set(initialValues);
        this.sortSet();
    }

    sortSet() {
        const sortedArray = Array.from(this.set).sort((a, b) => a - b);
        this.set = new Set(sortedArray);
    }

    add(value) {
        this.set.add(value);
        this.sortSet();
    }

    remove(value) {
        this.set.delete(value);
    }

    contains(value) {
        return this.set.has(value);
    }

    toArray() {
        return Array.from(this.set);
    }

    print() {
        const elements = Array.from(this.set).join(', ');
        console.log("[" + elements + "]");
    }    
}

function func_sortedset() {
    // Create a Set with initial elements
    const sortedSet = new SortedSet([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);

    // Loop and print each element
    console.log("SortedSet elements:");
    sortedSet.print();

    // Adding new elements (one duplicate and one new) to the sortedSet
    sortedSet.add(5);
    sortedSet.add(12);
    sortedSet.add(11);

    // Removing an element from the sortedSet
    sortedSet.remove(8);

    // Get the value by index is not applicable in sortedSets since the elements have no order

    // Check if a value exists in the sortedSet
    const exists = sortedSet.contains(4);
    console.log("\nElement with value 4 is existent in sortedSet: " + exists);

    // Print each element after modifications
    console.log("\nSortedSet elements after modifications:");
    sortedSet.print();
}

class Queue {
    constructor(initialValues = []) {
        this.queue = [...initialValues];
    }

    enqueue(value) {
        this.queue.push(value);
    }

    dequeue() {
        return this.queue.shift();
    }

    contains(value) {
        return this.queue.includes(value);
    }

    print() {
        console.log(this.queue);
    }
}

function func_queue() {
    const queue = new Queue([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);

    // Loop and print all elements of queue
    console.log("Queue elements:");
    queue.print();

    // Add element to the queue
    queue.enqueue(11);
    
    // Remove element from the queue (FIFO)
    queue.dequeue();
    
    // Access elements by index in a queue is not applicable. To access a specific element, dequeue elements until desired element.

    const exists = queue.contains(4);
    console.log("\nElement with value 4 is existent in queue:", exists);

    console.log("\nQueue elements after modifications:");
    queue.print();
}

class Stack {
    constructor(initialValues = []) {
        this.stack = [...initialValues];
    }

    push(value) {
        this.stack.push(value);
    }

    pop() {
        return this.stack.pop();
    }

    contains(value) {
        return this.stack.includes(value);
    }

    print() {
        console.log(this.stack);
    }
}

function func_stack() {
    const stack = new Stack([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);

    // Loop and print all elements of stack
    console.log("Stack elements:");
    stack.print();

    // Add elements to the stack
    stack.push(11);
    stack.push(12);

    // Remove element from the stack (LIFO)
    stack.pop();
    
    // Access elements by index in a stack is not applicable. To access a specific element, pop elements until desired element.

    const exists = stack.contains(4);
    console.log("\nElement with value 4 is existent in stack:", exists);

    console.log("\nStack elements after modifications:");
    stack.print();
}

class Node {
    constructor(value) {
        this.value = value;
        this.next = null;
    }
}

class LinkedList {
    constructor(initialValues = []) {
        this.head = null;
        initialValues.forEach(value => this.addLast(value));
    }

    addLast(value) {
        const newNode = new Node(value);
        if (!this.head) {
            this.head = newNode;
        } else {
            let current = this.head;
            while (current.next) {
                current = current.next;
            }
            current.next = newNode;
        }
    }

    find(value) {
        let current = this.head;
        while (current) {
            if (current.value === value) {
                return current;
            }
            current = current.next;
        }
        return null;
    }

    remove(nodeToRemove) {
        if (!this.head) return;
        if (this.head === nodeToRemove) {
            this.head = this.head.next;
            return;
        }
        let current = this.head;
        while (current.next) {
            if (current.next === nodeToRemove) {
                current.next = current.next.next;
                return;
            }
            current = current.next;
        }
    }

    contains(value) {
        let current = this.head;
        while (current) {
            if (current.value === value) {
                return true;
            }
            current = current.next;
        }
        return false;
    }

    print() {
        let current = this.head;
        const elements = [];
        while (current) {
            elements.push(current.value);
            current = current.next;
        }
        console.log(elements);
    }
}

function func_linkedlist() {
    const linkedList = new LinkedList([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);

    // Loop and print elements
    console.log("LinkedList elements:");
    linkedList.print();

    // Add element to linkedList
    linkedList.addLast(11);

    // remove element from linked list
    const nodeToRemove = linkedList.find(8);
    if (nodeToRemove) {
        linkedList.remove(nodeToRemove);
    }

    // Check existence of element in linkedList
    const exists = linkedList.contains(4);
    console.log("\nElement with value 4 is existent in LinkedList:", exists);

    // Print each element after modification
    console.log("\nLinkedList elements:");
    linkedList.print();
}

// Call the function
//func_array();
//func_dictionary();
//func_sortedlist();
//func_hashset();
//func_sortedset();
//func_queue();
//func_stack();
func_linkedlist();

