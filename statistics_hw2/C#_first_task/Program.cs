﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Threading.Tasks.Dataflow;

class Program
{
    static void Main()
    {
        string[] variables1 = {"Programming Languages"}; // <- qualitative variable
        int[] variableTypes1 = {0}; // 0 = qualitative; 1 = quantitative

        SortedDictionary<string, int> distribution = GetJointDistribution(variables1, variableTypes1);
        print_distribution(distribution, variables1);

        string[] variables2 = {"Age"}; // <- discrete quantitative variable
        int[] variableTypes2 = {1}; // 0 = qualitative; 1 = quantitative

        distribution = GetJointDistribution(variables2, variableTypes2);
        print_distribution(distribution, variables2);

        string[] variables3 = {"height"}; // <- continuous quantitative variable
        int[] variableTypes3 = {1}; // 0 = qualitative; 1 = quantitative

        distribution = GetJointDistribution(variables3, variableTypes3);
        print_distribution(distribution, variables3);

        string[] variables4 = {"Age", "height"}; // <- discrete quantitative variable
        int[] variableTypes4 = {1, 1}; // 0 = qualitative; 1 = quantitative

        distribution = GetJointDistribution(variables4, variableTypes4);
        print_distribution(distribution, variables4);
    }

    static void print_distribution(SortedDictionary<string, int> distribution, string[] variables)
    {  
        int total = 0;
        
        //Total counts to calculate relative and percentage disribution values
        foreach (var item in distribution)
        {
            total += item.Value;
        }
        
        // Loop and print each element
        Console.WriteLine("Dictionary elements:");
        foreach (var item in distribution)
        {
            
            Console.WriteLine($"{string.Join(", ", variables)}: [{item.Key}], | Counts: (Abs: {item.Value}, Rel: {((double)item.Value)/total:F2}, Perc: {(((double)item.Value)/total)*100:F2} %)");
        }
        Console.WriteLine("-----------------------------------------------------\n");
    }
    static SortedDictionary<string, int> GetJointDistribution(string[] variables, int[] variableTypes)
    {
        // Reading CSV and processing single variables
        string filePath = "/Users/jcorth/Desktop/projects/statistics_hw2/Professional Life - Sheet1.csv";

        // Dictionary to store (joint) distribution
        SortedDictionary<string, int> jointDistribution = new SortedDictionary<string, int>();

        // Check if all variables exist on first line (header)
        string firstLine = File.ReadLines(filePath).FirstOrDefault();
        string[] variableFields = Regex.Split(firstLine, ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");

        int k = variables.Length;
        int[] variableIndexes = new int[k];
        bool exists;

        foreach (var item in variables)
        {
            exists = variableFields.Contains(item);
            Console.WriteLine($"Variable '{item}' exists: {exists}");

            // Get the index of all variables in variable_fields array
            variableIndexes[Array.IndexOf(variables, item)] = Array.IndexOf(variableFields, item);
        }
        Console.WriteLine("");
           
        try
        {
            foreach (string line in File.ReadLines(filePath).Skip(1))
            {
                // Split the line into fields using comma as the delimiter
                string[] fields = Regex.Split(line, ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                string[][] keysArray = new string[k][]; // Stores all valuesArray for each variable
                string[] valuesArray; // Stores all values given for one variable by each unit

                for (int i = 0; i < k; i++)
                {
                    string key;

                    if (variableTypes[i] == 1)
                    {
                        key = Regex.Replace(fields[variableIndexes[i]].Trim(), @"[^\d.]", "");
                    }
                    else
                    {
                        key = fields[variableIndexes[i]].Trim();
                        TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                        key = textInfo.ToTitleCase(key.ToLower());
                    }

                    valuesArray = key.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                         .Select(k => k.Trim().Trim('"'))
                         .ToArray();

                    keysArray[i] = valuesArray;
                    
                }

                List<string> combinations = GenerateCombinations(keysArray); // Get list of each possible combination of the particular unit

                foreach (var combination in combinations)
                {   
                    // Update joint distribution
                    if (!jointDistribution.ContainsKey(combination))
                    {
                        jointDistribution.Add(combination, 1);
                    }
                    else
                    {
                        jointDistribution[combination]++;
                    }
                } 
            }
        }
        catch (FileNotFoundException)
        {
            Console.WriteLine("File not found.");
        }
        catch (IOException)
        {
            Console.WriteLine("Error reading the file.");
        }
        catch (FormatException)
        {
            Console.WriteLine("Invalid format in the CSV file.");
        }
        Console.WriteLine("-----------------------------------------------------\n");
        return jointDistribution;
    }

    // Generates all possible combinations from all given values of each variable recursively
    static List<string> GenerateCombinations(string[][] valuesArray, int depth = 0, string currentCombination = "")
    {
        List<string> combinations = new List<string>();

        if (depth == valuesArray.Length) // Already reached end
        {
            combinations.Add(currentCombination.TrimEnd(','));
        }
        else
        {
            foreach (string value in valuesArray[depth])
            {
                List<string> subCombinations = GenerateCombinations(valuesArray, depth + 1, currentCombination + value + ",");
                foreach (var kvp in subCombinations)
                {
                    combinations.Add(kvp);
                }
            }
        }
        return combinations;
    }


}
