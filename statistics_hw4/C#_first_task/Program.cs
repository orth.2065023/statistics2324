﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Threading.Tasks.Dataflow;

class Program
{
    static void Main()
    {
        string[] variables1 = {"Programming Languages"}; // <- qualitative variable
        int[] variableTypes1 = {0}; // 0 = qualitative; 1 = quantitative

        SortedDictionary<string, int> distribution = GetJointDistribution(variables1, variableTypes1);
        print_distribution(distribution, variables1);

        string[] variables4 = {"Age", "height"}; // <- discrete quantitative variable
        int[] variableTypes4 = {1, 1}; // 0 = qualitative; 1 = quantitative

        // Get joint distribution
        distribution = GetJointDistribution(variables4, variableTypes4);
        print_distribution(distribution, variables4);

        // Now cluster joint distribution into specidied intervals

        // Specify intervals for keys
        Dictionary<string, Tuple<double, double, double>> keyIntervals = new Dictionary<string, Tuple<double, double, double>>
        {
            { "Age", new Tuple<double, double, double>(20, 25, 1) },
            { "height", new Tuple<double, double, double>(1.6, 2.2, 0.2) }
        };

        // Print specified intervals
        foreach (var interval in keyIntervals)
        {
            Console.WriteLine($"Interval for {interval.Key}: Start={interval.Value.Item1}, End={interval.Value.Item2}, Step={interval.Value.Item3}");
        }

        // Cluster the input data
        SortedDictionary<string, int> clusteredDictionary = ClusterData(distribution, keyIntervals);

        // Print clustered results
        Console.WriteLine("\nClustered Data:");
        foreach (var entry in clusteredDictionary)
        {
            Console.WriteLine($"[{entry.Key} + IntervallSteps): {entry.Value}");
        }

    }

    static void print_distribution(SortedDictionary<string, int> distribution, string[] variables)
    {  
        int total = 0;
        
        //Total counts to calculate relative and percentage disribution values
        foreach (var item in distribution)
        {
            total += item.Value;
        }
        
        // Loop and print each element
        Console.WriteLine("Dictionary elements:");
        foreach (var item in distribution)
        {
            
            Console.WriteLine($"{string.Join(", ", variables)}: [{item.Key}], | Counts: (Abs: {item.Value}, Rel: {((double)item.Value)/total:F2}, Perc: {(((double)item.Value)/total)*100:F2} %)");
        }
        Console.WriteLine("-----------------------------------------------------\n");
    }
    static SortedDictionary<string, int> GetJointDistribution(string[] variables, int[] variableTypes)
    {
        // Reading CSV and processing single variables
        string filePath = "/Users/jcorth/Desktop/projects/statistics_hw2/Professional Life - Sheet1.csv";

        // Dictionary to store (joint) distribution
        SortedDictionary<string, int> jointDistribution = new SortedDictionary<string, int>();

        // Check if all variables exist on first line (header)
        string firstLine = File.ReadLines(filePath).FirstOrDefault();
        string[] variableFields = Regex.Split(firstLine, ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");

        int k = variables.Length;
        int[] variableIndexes = new int[k];
        bool exists;

        foreach (var item in variables)
        {
            exists = variableFields.Contains(item);
            Console.WriteLine($"Variable '{item}' exists: {exists}");

            // Get the index of all variables in variable_fields array
            variableIndexes[Array.IndexOf(variables, item)] = Array.IndexOf(variableFields, item);
        }
        Console.WriteLine("");
           
        try
        {
            foreach (string line in File.ReadLines(filePath).Skip(1))
            {
                // Split the line into fields using comma as the delimiter
                string[] fields = Regex.Split(line, ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                string[][] keysArray = new string[k][]; // Stores all valuesArray for each variable
                string[] valuesArray; // Stores all values given for one variable by each unit

                for (int i = 0; i < k; i++)
                {
                    string key;

                    if (variableTypes[i] == 1)
                    {
                        key = Regex.Replace(fields[variableIndexes[i]].Trim(), @"[^\d.]", "");
                    }
                    else
                    {
                        key = fields[variableIndexes[i]].Trim();
                        TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                        key = textInfo.ToTitleCase(key.ToLower());
                    }

                    valuesArray = key.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                         .Select(k => k.Trim().Trim('"'))
                         .ToArray();

                    keysArray[i] = valuesArray;
                    
                }

                List<string> combinations = GenerateCombinations(keysArray); // Get list of each possible combination of the particular unit

                foreach (var combination in combinations)
                {   
                    // Update joint distribution
                    if (!jointDistribution.ContainsKey(combination))
                    {
                        jointDistribution.Add(combination, 1);
                    }
                    else
                    {
                        jointDistribution[combination]++;
                    }
                } 
            }
        }
        catch (FileNotFoundException)
        {
            Console.WriteLine("File not found.");
        }
        catch (IOException)
        {
            Console.WriteLine("Error reading the file.");
        }
        catch (FormatException)
        {
            Console.WriteLine("Invalid format in the CSV file.");
        }
        Console.WriteLine("-----------------------------------------------------\n");
        return jointDistribution;
    }

    // Generates all possible combinations from all given values of each variable recursively
    static List<string> GenerateCombinations(string[][] valuesArray, int depth = 0, string currentCombination = "")
    {
        List<string> combinations = new List<string>();

        if (depth == valuesArray.Length) // Already reached end
        {
            combinations.Add(currentCombination.TrimEnd(','));
        }
        else
        {
            foreach (string value in valuesArray[depth])
            {
                List<string> subCombinations = GenerateCombinations(valuesArray, depth + 1, currentCombination + value + ",");
                foreach (var kvp in subCombinations)
                {
                    combinations.Add(kvp);
                }
            }
        }
        return combinations;
    }


    // For each specified variable, cluster the variable based on start, end and intervalsteps
    static SortedDictionary<string, int> ClusterData(SortedDictionary<string, int> inputDictionary, Dictionary<string, Tuple<double, double, double>> keyIntervals)
    {
        SortedDictionary<string, int> clusteredDictionary = new SortedDictionary<string, int>();

        List<string> variableNames = new List<string>(keyIntervals.Keys);
        int numVariables = variableNames.Count;

        double[] intervalStarts = new double[numVariables];
        double[] intervalEnds = new double[numVariables];
        double[] intervalSteps = new double[numVariables];

        for (int i = 0; i < numVariables; i++)
        {
            intervalStarts[i] = keyIntervals[variableNames[i]].Item1;
            intervalEnds[i] = keyIntervals[variableNames[i]].Item2;
            intervalSteps[i] = keyIntervals[variableNames[i]].Item3;
        }

        // Recursive function to generate all possible keys
        ClusterRecursive(variableNames, intervalStarts, intervalEnds, intervalSteps, 0, "", clusteredDictionary);

        foreach (var entry in inputDictionary)
        {
            string[] keys = entry.Key.Split(',');
            string clusteredKey = "";

            for (int i = 0; i < numVariables; i++)
            {
                double variableValue = double.Parse(keys[i], CultureInfo.InvariantCulture); // Parse with decimal precision

                double clusteredVariable = intervalStarts[i] + (int)((variableValue - intervalStarts[i]) / intervalSteps[i] ) * intervalSteps[i]; // Adjusted clusteredHeight calculation

                if (clusteredKey.Length > 0)
                {
                    clusteredKey = $"{clusteredKey},{(clusteredVariable % 1 == 0 ? $"{clusteredVariable:0}" : $"{clusteredVariable.ToString("0.0", CultureInfo.InvariantCulture):0.0}")}";
                }
                else
                {
                    clusteredKey = $"{(clusteredVariable % 1 == 0 ? $"{clusteredVariable:0}" : $"{clusteredVariable.ToString("0.0", CultureInfo.InvariantCulture):0.0}")}";
                }
            }

            if (clusteredDictionary.ContainsKey(clusteredKey))
            {
                clusteredDictionary[clusteredKey] += entry.Value;
            }
        }



        return clusteredDictionary;
    }


    // Checks if max depth is reached, if not, add value of current variable interval to current key and calls itself again
    static void ClusterRecursive(List<string> variableNames, double[] intervalStarts, double[] intervalEnds, double[] intervalSteps, int index, string currentKey, SortedDictionary<string, int> clusteredDictionary)
    {
        if (index == variableNames.Count)
        {
            if (!clusteredDictionary.ContainsKey(currentKey))
            {
                clusteredDictionary.Add(currentKey, 0);
            }
            return;
        }

        for (double value = intervalStarts[index]; value <= intervalEnds[index]; value += intervalSteps[index])
        {
            string newKey;
            value.ToString("0.0", CultureInfo.InvariantCulture);
            if (currentKey.Length > 0)
            {
                newKey = $"{currentKey},{(value % 1 == 0 ? $"{value:0}" : $"{value.ToString("0.0", CultureInfo.InvariantCulture):0.0}")}";
            }
            else
            {
                newKey = $"{(value % 1 == 0 ? $"{value:0}" : $"{value.ToString("0.0", CultureInfo.InvariantCulture):0.0}")}";
            }


            ClusterRecursive(variableNames, intervalStarts, intervalEnds, intervalSteps, index + 1, newKey, clusteredDictionary);
        }
    }
}
