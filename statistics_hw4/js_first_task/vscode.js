"use strict";

// Main function to orchestrate the process
function main() {

    const variables1 = ["Programming Languages"]; // <- discrete quantitative variable
    const variableTypes1 = [0]; // 0 = qualitative; 1 = quantitative

    // Get joint distribution from CSV file
    var distribution = getJointDistribution(variables1, variableTypes1);
    // Print the original distribution
    printDistribution(distribution, variables1);

    // Define variables and their types
    const variables4 = ["Age", "height"]; // <- discrete quantitative variable
    const variableTypes4 = [1, 1]; // 0 = qualitative; 1 = quantitative

    // Get joint distribution from CSV file
    distribution = getJointDistribution(variables4, variableTypes4);
    // Print the original distribution
    printDistribution(distribution, variables4);

    // Define intervals for clustering
    const keyIntervals = {
        "Age": [20, 30, 2], // start -> 20, end-> 30, step -> 2
        "height": [1.0, 2.0, 0.2],
    };

    // Print specified intervals
    for (const key in keyIntervals) {
        console.log(`Interval for ${key}: Start=${keyIntervals[key][0]}, End=${keyIntervals[key][1]}, Step=${keyIntervals[key][2]}`);
    }

    // Cluster the data based on specified intervals
    const clusteredDictionary = clusterData(distribution, keyIntervals);

    // Print clustered results
    console.log("\nClustered Data:");
    for (const key in clusteredDictionary) {
        console.log(`[${key} + IntervallSteps): ${clusteredDictionary[key]}`);
    }
}

// Print the joint distribution
function printDistribution(distribution, variables) {
    let total = 0;

    // Calculate total counts for relative and percentage distribution
    for (const item in distribution) {
        total += distribution[item];
    }

    // Print each element in the distribution
    console.log("Dictionary elements:");
    for (const item in distribution) {
        console.log(`${variables.join(", ")}: [${item}], | Counts: (Abs: ${distribution[item]}, Rel: ${(distribution[item] / total).toFixed(2)}, Perc: ${((distribution[item] / total) * 100).toFixed(2)} %)`);
    }
    console.log("-----------------------------------------------------\n");
}

// Get joint distribution from the CSV file
function getJointDistribution(variables, variableTypes) {
    // Read CSV file and process variables
    const filePath = "/Users/jcorth/Desktop/projects/statistics_hw2/Professional Life - Sheet1.csv";

    // Dictionary to store joint distribution
    const jointDistribution = {};

    // Check if variables exist in the header
    const firstLine = readFileLines(filePath)[0];
    const variableFields = firstLine.split(/,(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)/);

    const k = variables.length;
    const variableIndexes = new Array(k);
    let exists;

    // Check existence of variables and get their indexes
    variables.forEach((item) => {
        exists = variableFields.includes(item);
        console.log(`Variable '${item}' exists: ${exists}`);
        variableIndexes[variables.indexOf(item)] = variableFields.indexOf(item);
    });
    console.log("");

    try {
        // Read lines from the file, skip header line
        const lines = readFileLines(filePath).slice(1);

        lines.forEach((line) => {
            // Split the line into fields using comma as the delimiter, handling double quotes
            const fields = line.split(/,(?=(?:[^"]*"[^"]*")*[^"]*$)/);
            const keysArray = new Array(k); // Store values for each variable
            let valuesArray;

            for (let i = 0; i < k; i++) {
                let key;

                // Process variables based on their types
                if (variableTypes[i] === 1) {
                    key = fields[variableIndexes[i]].trim().replace(/[^\d.]/g, "");
                } else {
                    key = fields[variableIndexes[i]].trim().replace(/"/g, '').toLowerCase();
                    key = key.split(' ').map(word => word.charAt(0).toUpperCase() + word.slice(1)).join(' ');
                }

                // Split, trim, and remove quotes, then filter out empty entries
                valuesArray = key.split(',').map(k => k.trim().replace(/"/g, '')).filter(k => k !== '');
                keysArray[i] = valuesArray;
            }

            const combinations = generateCombinations(keysArray); // Generate all possible combinations of variables

            combinations.forEach((combination) => {
                // Update joint distribution
                if (!jointDistribution[combination]) {
                    jointDistribution[combination] = 1;
                } else {
                    jointDistribution[combination]++;
                }
            });
        });
    } catch (error) {
        // Handle file-related errors
        if (error.name === "FileNotFoundError") {
            console.log("File not found.");
        } else if (error.name === "IOException") {
            console.log("Error reading the file.");
        } else if (error.name === "FormatException") {
            console.log("Invalid format in the CSV file.");
        }
    }
    console.log("-----------------------------------------------------\n");

    // sort distribution keys alphabetically
    const sortedJointDistribution = Object.fromEntries(
        Object.entries(jointDistribution).sort((a, b) => a[0].localeCompare(b[0]))
    );
    return sortedJointDistribution;
}

// Generate all possible combinations of variables
function generateCombinations(valuesArray, depth = 0, currentCombination = "") {
    const combinations = [];

    if (depth === valuesArray.length) {
        // Already reached the end
        combinations.push(currentCombination.replace(/,$/, ""));
    } else {
        valuesArray[depth].forEach((value) => {
            const subCombinations = generateCombinations(valuesArray, depth + 1, currentCombination + value + ",");
            subCombinations.forEach((kvp) => {
                combinations.push(kvp);
            });
        });
    }
    return combinations;
}

// Cluster data based on specified intervals
function clusterData(inputDictionary, keyIntervals) {
    const clusteredDictionary = {};

    const variableNames = Object.keys(keyIntervals);
    const numVariables = variableNames.length;

    const intervalStarts = new Array(numVariables);
    const intervalEnds = new Array(numVariables);
    const intervalSteps = new Array(numVariables);

    for (let i = 0; i < numVariables; i++) {
        intervalStarts[i] = keyIntervals[variableNames[i]][0];
        intervalEnds[i] = keyIntervals[variableNames[i]][1];
        intervalSteps[i] = keyIntervals[variableNames[i]][2];
    }

    // Recursive function to generate all possible keys
    clusterRecursive(variableNames, intervalStarts, intervalEnds, intervalSteps, 0, "", clusteredDictionary);

    for (const entry in inputDictionary) {
        const keys = entry.split(",");
        let clusteredKey = "";

        for (let i = 0; i < numVariables; i++) {
            const variableValue = parseFloat(keys[i]); // Parse with decimal precision

            const clusteredVariable =
                intervalStarts[i] + Math.floor((variableValue - intervalStarts[i]) / intervalSteps[i]) * intervalSteps[i];

            if (clusteredKey.length > 0) {
                clusteredKey += `,${clusteredVariable % 1 === 0 ? clusteredVariable.toFixed(0) : clusteredVariable.toFixed(1)}`;
            } else {
                clusteredKey += clusteredVariable % 1 === 0 ? clusteredVariable.toFixed(0) : clusteredVariable.toFixed(1);
            }
        }

        if (clusteredKey in clusteredDictionary) {
            clusteredDictionary[clusteredKey] += inputDictionary[entry];
        }
        
    }

    return clusteredDictionary;
}

// Recursive function to cluster variables based on intervals
function clusterRecursive(variableNames, intervalStarts, intervalEnds, intervalSteps, index, currentKey, clusteredDictionary) {
    if (index === variableNames.length) {
        // Max depth reached, update clustered dictionary
        if (!clusteredDictionary[currentKey]) {
            clusteredDictionary[currentKey] = 0;
        }
        return;
    }

    for (let value = intervalStarts[index]; value <= intervalEnds[index]; value += intervalSteps[index]) {
        const newKey = currentKey.length > 0 ? `${currentKey},${value % 1 === 0 ? value.toFixed(0) : value.toFixed(1)}` : `${value % 1 === 0 ? value.toFixed(0) : value.toFixed(1)}`;

        // Recursively generate keys for next variable
        clusterRecursive(variableNames, intervalStarts, intervalEnds, intervalSteps, index + 1, newKey, clusteredDictionary);
    }
}

// Read file lines using fs module
const fs = require('fs');

function readFileLines(filePath) {
    try {
        // Read file synchronously and split lines
        const fileContent = fs.readFileSync(filePath, 'utf8');
        return fileContent.split('\n');
    } catch (error) {
        // Handle file read error
        console.error('Error reading the file:', error.message);
        return [];
    }
}

// Invoke the main function to start the process
main();
